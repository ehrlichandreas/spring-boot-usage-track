package com.cognizant.ehrlichandreas.usagetrack.resource;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;
import static org.springframework.test.context.jdbc.SqlConfig.TransactionMode.ISOLATED;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.cognizant.ehrlichandreas.usagetrack.configuration.JacksonConfiguration;
import com.cognizant.ehrlichandreas.usagetrack.ressource.UsageTrack;
import com.cognizant.ehrlichandreas.usagetrack.ressource.UsageTrack.UsageTrackBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
@WithMockUser
@AutoConfigureMockMvc(secure = false)
@Sql(
    scripts = "/sql/truncate-all-tables.sql",
    config = @SqlConfig(transactionMode = ISOLATED),
    executionPhase = BEFORE_TEST_METHOD
)
@Sql(
    scripts = "/sql/truncate-all-tables.sql",
    config = @SqlConfig(transactionMode = ISOLATED),
    executionPhase = AFTER_TEST_METHOD
)
@ActiveProfiles("insecure")
public class UsagesResourceTest {

    private static final String RESOURCE_TYPE = "resourceType";
    private static final String RESOURCE_CODE = "resourceCode";
    private static final String CONSUMER_ID = "consumerId";
    private static final String EVENT_ID = "eventId";
    private static final String URL_GET_TEMPLATE =
            "/api/resource-usages/types/resourceType/codes/resourceCode/consumers/consumerId";
    private static final String URL_POST_TEMPLATE = "/api/resource-usages";
    private static final String JSON_RESOURCE_TYPE = "$.resource_type";
    private static final String JSON_RESOURCE_CODE = "$.resource_code";
    private static final String JSON_CONSUMER_ID = "$.consumer_id";
    private static final String JSON_USAGES_CONSUMER = "$.usages_consumer";
    private static final String JSON_USAGES_TOTAL = "$.usages_total";
    private static final String TEST_USER = "test-user";

    @Autowired private MockMvc mockMvc;
    @Autowired private JacksonConfiguration jacksonConfiguration;

    @Test
    public void handlesUnknownParametersAsUnused() throws Exception {
        mockMvc.perform(get(URL_GET_TEMPLATE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_RESOURCE_TYPE, is(RESOURCE_TYPE)))
                .andExpect(jsonPath(JSON_RESOURCE_CODE, is(RESOURCE_CODE)))
                .andExpect(jsonPath(JSON_CONSUMER_ID, is(CONSUMER_ID)))
                .andExpect(jsonPath(JSON_USAGES_CONSUMER, is(0)))
                .andExpect(jsonPath(JSON_USAGES_TOTAL, is(0)));
    }

    @Test
    public void returnsConsumerSpecificUsages() throws Exception {
        UsageTrack usageTrack =
                UsageTrackBuilder.anUsageTrack()
                        .withConsumerId(CONSUMER_ID)
                        .withEventId(EVENT_ID)
                        .withResourceCode(RESOURCE_CODE)
                        .withResourceType(RESOURCE_TYPE)
                        .build();
        String usageTrackJson = jacksonConfiguration.objectMapper().writeValueAsString(usageTrack);

        mockMvc.perform(
                        post(URL_POST_TEMPLATE)
                                .with(user(TEST_USER))
                                .content(usageTrackJson)
                                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated())
                .andExpect(content().string(""));

        mockMvc.perform(get(URL_GET_TEMPLATE).with(user(TEST_USER)))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_RESOURCE_TYPE, is(RESOURCE_TYPE)))
                .andExpect(jsonPath(JSON_RESOURCE_CODE, is(RESOURCE_CODE)))
                .andExpect(jsonPath(JSON_CONSUMER_ID, is(CONSUMER_ID)))
                .andExpect(jsonPath(JSON_USAGES_CONSUMER, is(1)))
                .andExpect(jsonPath(JSON_USAGES_TOTAL, is(1)));

        mockMvc.perform(get(URL_GET_TEMPLATE + 2).with(user(TEST_USER)))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_RESOURCE_TYPE, is(RESOURCE_TYPE)))
                .andExpect(jsonPath(JSON_RESOURCE_CODE, is(RESOURCE_CODE)))
                .andExpect(jsonPath(JSON_CONSUMER_ID, is(CONSUMER_ID + 2)))
                .andExpect(jsonPath(JSON_USAGES_CONSUMER, is(0)))
                .andExpect(jsonPath(JSON_USAGES_TOTAL, is(1)));
    }

    @Test
    public void incrementsUsageAmountOnlyForDifferentEvents() throws Exception {
        ObjectMapper objectMapper = jacksonConfiguration.objectMapper();

        UsageTrackBuilder usageTrackBuilder =
                UsageTrackBuilder.anUsageTrack()
                        .withConsumerId(CONSUMER_ID)
                        .withEventId(EVENT_ID)
                        .withResourceCode(RESOURCE_CODE)
                        .withResourceType(RESOURCE_TYPE);

        UsageTrack usageTrack1 = usageTrackBuilder.build();
        UsageTrack usageTrack2 = usageTrackBuilder.but().withConsumerId(CONSUMER_ID + 2).build();
        UsageTrack usageTrack3 = usageTrackBuilder.but().withEventId(EVENT_ID + 2).build();
        UsageTrack usageTrack4 =
                usageTrackBuilder
                        .but()
                        .withConsumerId(CONSUMER_ID + 3)
                        .withEventId(EVENT_ID + 3)
                        .build();

        String usageTrackJson1 = objectMapper.writeValueAsString(usageTrack1);
        String usageTrackJson2 = objectMapper.writeValueAsString(usageTrack2);
        String usageTrackJson3 = objectMapper.writeValueAsString(usageTrack3);
        String usageTrackJson4 = objectMapper.writeValueAsString(usageTrack4);

        MockHttpServletRequestBuilder requestBuilder =
                post(URL_POST_TEMPLATE)
                        .with(user(TEST_USER))
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);

        mockMvc.perform(requestBuilder.content(usageTrackJson1))
                .andExpect(status().isCreated())
                .andExpect(content().string(""));

        mockMvc.perform(requestBuilder.content(usageTrackJson2))
                .andExpect(status().isConflict())
                .andExpect(content().string(""));

        mockMvc.perform(requestBuilder.content(usageTrackJson3))
                .andExpect(status().isCreated())
                .andExpect(content().string(""));

        mockMvc.perform(requestBuilder.content(usageTrackJson4))
                .andExpect(status().isCreated())
                .andExpect(content().string(""));

        mockMvc.perform(get(URL_GET_TEMPLATE).with(user(TEST_USER)))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_RESOURCE_TYPE, is(RESOURCE_TYPE)))
                .andExpect(jsonPath(JSON_RESOURCE_CODE, is(RESOURCE_CODE)))
                .andExpect(jsonPath(JSON_CONSUMER_ID, is(CONSUMER_ID)))
                .andExpect(jsonPath(JSON_USAGES_CONSUMER, is(2)))
                .andExpect(jsonPath(JSON_USAGES_TOTAL, is(3)));

        mockMvc.perform(get(URL_GET_TEMPLATE + 2).with(user(TEST_USER)))
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_RESOURCE_TYPE, is(RESOURCE_TYPE)))
                .andExpect(jsonPath(JSON_RESOURCE_CODE, is(RESOURCE_CODE)))
                .andExpect(jsonPath(JSON_CONSUMER_ID, is(CONSUMER_ID + 2)))
                .andExpect(jsonPath(JSON_USAGES_CONSUMER, is(0)))
                .andExpect(jsonPath(JSON_USAGES_TOTAL, is(3)));
    }
}
