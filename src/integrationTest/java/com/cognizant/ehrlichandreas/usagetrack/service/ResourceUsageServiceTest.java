package com.cognizant.ehrlichandreas.usagetrack.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;
import static org.springframework.test.context.jdbc.SqlConfig.TransactionMode.ISOLATED;

import com.cognizant.ehrlichandreas.usagetrack.ressource.UsageInformation;
import com.cognizant.ehrlichandreas.usagetrack.ressource.UsageTrack;
import com.cognizant.ehrlichandreas.usagetrack.ressource.UsageTrack.UsageTrackBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Sql(
    scripts = "/sql/truncate-all-tables.sql",
    config = @SqlConfig(transactionMode = ISOLATED),
    executionPhase = BEFORE_TEST_METHOD
)
@Sql(
    scripts = "/sql/truncate-all-tables.sql",
    config = @SqlConfig(transactionMode = ISOLATED),
    executionPhase = AFTER_TEST_METHOD
)
public class ResourceUsageServiceTest {

    private static final String RESOURCE_TYPE = "resourceType";
    private static final String RESOURCE_CODE = "resourceCode";
    private static final String CONSUMER_ID = "consumerId";
    private static final String EVENT_ID = "eventId";

    @Autowired private ResourceUsageService usageService;

    @Test
    public void returnsZeroConsumerUsageAmount() {
        assertThat(usageService.getUsageInformation(RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID))
                .isEqualToComparingFieldByField(
                        new UsageInformation()
                                .setUsagesTotal(0)
                                .setUsagesConsumer(0)
                                .setConsumerId(CONSUMER_ID)
                                .setResourceCode(RESOURCE_CODE)
                                .setResourceType(RESOURCE_TYPE));
    }

    @Test
    public void incrementsUsageAmount() {
        UsageTrack usageTrack =
                UsageTrackBuilder.anUsageTrack()
                        .withConsumerId(CONSUMER_ID)
                        .withEventId(EVENT_ID)
                        .withResourceCode(RESOURCE_CODE)
                        .withResourceType(RESOURCE_TYPE)
                        .build();

        assertThat(usageService.incrementUsages(usageTrack)).isEqualTo(true);

        assertThat(usageService.getUsageInformation(RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID))
                .isEqualToComparingFieldByField(
                        new UsageInformation()
                                .setUsagesTotal(1)
                                .setUsagesConsumer(1)
                                .setConsumerId(CONSUMER_ID)
                                .setResourceCode(RESOURCE_CODE)
                                .setResourceType(RESOURCE_TYPE));
    }

    @Test
    public void incrementsUsageAmountOnlyForDifferentEvents() {
        UsageTrackBuilder usageTrackBuilder =
                UsageTrackBuilder.anUsageTrack()
                        .withConsumerId(CONSUMER_ID)
                        .withEventId(EVENT_ID)
                        .withResourceCode(RESOURCE_CODE)
                        .withResourceType(RESOURCE_TYPE);

        UsageTrack usageTrack1 = usageTrackBuilder.build();
        UsageTrack usageTrack2 = usageTrackBuilder.but().withConsumerId(CONSUMER_ID + 2).build();
        UsageTrack usageTrack3 = usageTrackBuilder.but().withEventId(EVENT_ID + 2).build();
        UsageTrack usageTrack4 =
                usageTrackBuilder
                        .but()
                        .withConsumerId(CONSUMER_ID + 3)
                        .withEventId(EVENT_ID + 3)
                        .build();

        assertThat(usageService.incrementUsages(usageTrack1)).isTrue();
        assertThat(usageService.incrementUsages(usageTrack2)).isFalse();
        assertThat(usageService.incrementUsages(usageTrack3)).isTrue();
        assertThat(usageService.incrementUsages(usageTrack4)).isTrue();

        assertThat(usageService.getUsageInformation(RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID))
                .isEqualToComparingFieldByField(
                        new UsageInformation()
                                .setUsagesTotal(3)
                                .setUsagesConsumer(2)
                                .setConsumerId(CONSUMER_ID)
                                .setResourceCode(RESOURCE_CODE)
                                .setResourceType(RESOURCE_TYPE));
    }
}
