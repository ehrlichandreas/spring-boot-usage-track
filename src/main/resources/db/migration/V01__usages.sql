CREATE TABLE IF NOT EXISTS ppu_data.resource_usages (
  resource_type   VARCHAR(36)   NOT NULL,
  resource_code   VARCHAR(36)   NOT NULL,
  consumer_id     VARCHAR(36)   NOT NULL,
  event_id        TEXT          NOT NULL,

  PRIMARY KEY (event_id)
);

CREATE INDEX code_consumer  ON ppu_data.resource_usages(resource_type, resource_code, consumer_id);


CREATE TABLE IF NOT EXISTS ppu_data.resource_usages_count (
  resource_type   VARCHAR(36)   NOT NULL,
  resource_code   VARCHAR(36)   NOT NULL,
  consumer_id     VARCHAR(36)   NOT NULL,
  usages          INT           NOT NULL DEFAULT 0,

  PRIMARY KEY (resource_type, resource_code, consumer_id)
);


CREATE TABLE IF NOT EXISTS ppu_data.total_usages_count (
  resource_type   VARCHAR(36)   NOT NULL,
  resource_code   VARCHAR(36)   NOT NULL,
  usages          INT           NOT NULL DEFAULT 0,

  PRIMARY KEY (resource_type, resource_code)
);
