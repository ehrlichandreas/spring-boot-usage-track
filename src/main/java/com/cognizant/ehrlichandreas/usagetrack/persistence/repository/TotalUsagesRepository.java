package com.cognizant.ehrlichandreas.usagetrack.persistence.repository;

import com.cognizant.ehrlichandreas.usagetrack.persistence.model.TotalUsageCount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

/** Repository to save total usages. */
public interface TotalUsagesRepository extends Repository<TotalUsageCount, String> {

    /** Uses a native postgres query to increment the total usage counter in only one call. */
    @Query(
        value =
                "INSERT INTO total_usages_count as t "
                        + "(resource_type, resource_code, usages) "
                        + "VALUES "
                        + "(:resourceType, :resourceCode, 1) "
                        + "ON CONFLICT (resource_type, resource_code) "
                        + "DO UPDATE SET "
                        + "usages = t.usages + 1 "
                        + "WHERE "
                        + "t.resource_type = :resourceType AND "
                        + "t.resource_code = :resourceCode "
                        + "RETURNING 1 ",
        nativeQuery = true
    )
    void increment(
            @Param("resourceType") String resourceType, @Param("resourceCode") String resourceCode);

    /** Returns the total amount of events tracked for the specified resource type and code. */
    @Query(
        value =
                "SELECT usages FROM total_usages_count as t WHERE "
                        + "t.resource_type = :resourceType AND "
                        + "t.resource_code = :resourceCode ",
        nativeQuery = true
    )
    Integer getTotalUsages(
            @Param("resourceType") String resourceType, @Param("resourceCode") String resourceCode);
}
