package com.cognizant.ehrlichandreas.usagetrack.persistence.model;

import com.google.common.base.MoreObjects;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "resource_usages")
public class ResourceUsage {

    @Column(name = "resource_type")
    private String resourceType;

    @Column(name = "resource_code")
    private String resourceCode;

    @Column(name = "consumer_id")
    private String consumerId;

    @Id
    @Column(name = "event_id")
    private String eventId;

    public String getResourceType() {
        return resourceType;
    }

    public ResourceUsage setResourceType(String resourceType) {
        this.resourceType = resourceType;
        return this;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public ResourceUsage setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
        return this;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public ResourceUsage setConsumerId(String consumerId) {
        this.consumerId = consumerId;
        return this;
    }

    public String getEventId() {
        return eventId;
    }

    public ResourceUsage setEventId(String eventId) {
        this.eventId = eventId;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResourceUsage)) return false;
        ResourceUsage that = (ResourceUsage) o;
        return Objects.equals(getResourceType(), that.getResourceType())
                && Objects.equals(getResourceCode(), that.getResourceCode())
                && Objects.equals(getConsumerId(), that.getConsumerId())
                && Objects.equals(getEventId(), that.getEventId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getResourceType(), getResourceCode(), getConsumerId(), getEventId());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("resourceType", resourceType)
                .add("resourceCode", resourceCode)
                .add("consumerId", consumerId)
                .add("eventId", eventId)
                .toString();
    }
}
