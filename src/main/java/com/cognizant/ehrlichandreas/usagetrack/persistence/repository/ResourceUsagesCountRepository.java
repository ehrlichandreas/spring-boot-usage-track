package com.cognizant.ehrlichandreas.usagetrack.persistence.repository;

import com.cognizant.ehrlichandreas.usagetrack.persistence.model.ResourceUsageCount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

/** Repository to save resource usages. */
public interface ResourceUsagesCountRepository extends Repository<ResourceUsageCount, String> {

    /** Uses a native postgres query to increment the ressource usage counter in only one call. */
    @Query(
        value =
                "INSERT INTO resource_usages_count as t "
                        + "(resource_type, resource_code, consumer_id, usages) "
                        + "VALUES "
                        + "(:resourceType, :resourceCode, :consumerId, 1) "
                        + "ON CONFLICT (resource_type, resource_code, consumer_id) "
                        + "DO UPDATE SET "
                        + "usages = t.usages + 1 "
                        + "WHERE "
                        + "t.resource_type = :resourceType AND "
                        + "t.resource_code = :resourceCode AND "
                        + "t.consumer_id = :consumerId "
                        + "RETURNING 1 ",
        nativeQuery = true
    )
    void increment(
            @Param("resourceType") String resourceType,
            @Param("resourceCode") String resourceCode,
            @Param("consumerId") String consumerId);

    /** Returns the total amount of events tracked for the specified resource type and code. */
    @Query(
        value =
                "SELECT usages FROM resource_usages_count as t WHERE "
                        + "t.resource_type = :resourceType AND "
                        + "t.resource_code = :resourceCode AND "
                        + "t.consumer_id = :consumerId ",
        nativeQuery = true
    )
    Integer getUsagesCount(
            @Param("resourceType") String resourceType,
            @Param("resourceCode") String resourceCode,
            @Param("consumerId") String consumerId);
}
