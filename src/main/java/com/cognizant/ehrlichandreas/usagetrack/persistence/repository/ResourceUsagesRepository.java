package com.cognizant.ehrlichandreas.usagetrack.persistence.repository;

import com.cognizant.ehrlichandreas.usagetrack.persistence.model.ResourceUsage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

/** Repository to save resource usages. */
public interface ResourceUsagesRepository extends Repository<ResourceUsage, String> {

    /**
     * Stores the resource usage information in an idempotent fashion. Returns {@code 1} if the
     * query resulted in an update, {@code null} otherwise.
     */
    @Query(
        nativeQuery = true,
        value =
                "INSERT INTO resource_usages "
                        + "(resource_type, resource_code, consumer_id, event_id) "
                        + "VALUES "
                        + "(:resourceType, :resourceCode, :consumerId, :eventId) "
                        + "ON CONFLICT (event_id) "
                        + "DO NOTHING "
                        + "RETURNING 1 "
    )
    Integer create(
            @Param("resourceType") String resourceType,
            @Param("resourceCode") String resourceCode,
            @Param("consumerId") String consumerId,
            @Param("eventId") String eventId);
}
