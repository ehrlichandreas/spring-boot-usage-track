package com.cognizant.ehrlichandreas.usagetrack.persistence.model;

import com.google.common.base.MoreObjects;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "resource_usages_count")
public class ResourceUsageCount {

    @Id
    @Column(name = "resource_type")
    private String resourceType;

    @Column(name = "resource_code")
    private String resourceCode;

    @Column(name = "consumer_id")
    private String consumerId;

    @Column(name = "usages")
    private Integer usages;

    public String getResourceType() {
        return resourceType;
    }

    public ResourceUsageCount setResourceType(String resourceType) {
        this.resourceType = resourceType;
        return this;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public ResourceUsageCount setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
        return this;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public ResourceUsageCount setConsumerId(String consumerId) {
        this.consumerId = consumerId;
        return this;
    }

    public Integer getUsages() {
        return usages;
    }

    public ResourceUsageCount setUsages(Integer usages) {
        this.usages = usages;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResourceUsageCount)) return false;
        ResourceUsageCount that = (ResourceUsageCount) o;
        return Objects.equals(getResourceType(), that.getResourceType())
                && Objects.equals(getResourceCode(), that.getResourceCode())
                && Objects.equals(getConsumerId(), that.getConsumerId())
                && Objects.equals(getUsages(), that.getUsages());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getResourceType(), getResourceCode(), getConsumerId(), getUsages());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("resourceType", resourceType)
                .add("resourceCode", resourceCode)
                .add("consumerId", consumerId)
                .add("usages", usages)
                .toString();
    }
}
