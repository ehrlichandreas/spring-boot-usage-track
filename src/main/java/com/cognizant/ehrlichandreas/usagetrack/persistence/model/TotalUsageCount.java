package com.cognizant.ehrlichandreas.usagetrack.persistence.model;

import com.google.common.base.MoreObjects;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "total_usages_count")
public class TotalUsageCount {

    @Id
    @Column(name = "resource_type")
    private String resourceType;

    @Column(name = "resource_code")
    private String resourceCode;

    @Column(name = "usages")
    private Integer usages;

    public String getResourceType() {
        return resourceType;
    }

    public TotalUsageCount setResourceType(String resourceType) {
        this.resourceType = resourceType;
        return this;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public TotalUsageCount setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
        return this;
    }

    public Integer getUsages() {
        return usages;
    }

    public TotalUsageCount setUsages(Integer usages) {
        this.usages = usages;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TotalUsageCount)) return false;
        TotalUsageCount that = (TotalUsageCount) o;
        return Objects.equals(getResourceType(), that.getResourceType())
                && Objects.equals(getResourceCode(), that.getResourceCode())
                && Objects.equals(getUsages(), that.getUsages());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getResourceType(), getResourceCode(), getUsages());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("resourceType", resourceType)
                .add("resourceCode", resourceCode)
                .add("usages", usages)
                .toString();
    }
}
