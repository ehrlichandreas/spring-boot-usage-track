package com.cognizant.ehrlichandreas.usagetrack.ressource;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.MoreObjects;
import java.util.Objects;

@JsonSerialize
@JsonDeserialize
public class UsageTrack {

    private String resourceType;

    private String resourceCode;

    private String consumerId;

    private String eventId;

    public String getResourceType() {
        return resourceType;
    }

    public UsageTrack setResourceType(String resourceType) {
        this.resourceType = resourceType;
        return this;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public UsageTrack setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
        return this;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public UsageTrack setConsumerId(String consumerId) {
        this.consumerId = consumerId;
        return this;
    }

    public String getEventId() {
        return eventId;
    }

    public UsageTrack setEventId(String eventId) {
        this.eventId = eventId;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UsageTrack)) return false;
        UsageTrack that = (UsageTrack) o;
        return Objects.equals(getResourceType(), that.getResourceType())
                && Objects.equals(getResourceCode(), that.getResourceCode())
                && Objects.equals(getConsumerId(), that.getConsumerId())
                && Objects.equals(getEventId(), that.getEventId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getResourceType(), getResourceCode(), getConsumerId(), getEventId());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("resourceType", resourceType)
                .add("resourceCode", resourceCode)
                .add("consumerId", consumerId)
                .add("eventId", eventId)
                .toString();
    }

    public static final class UsageTrackBuilder {

        private String resourceType;
        private String resourceCode;
        private String consumerId;
        private String eventId;

        private UsageTrackBuilder() {}

        public static UsageTrackBuilder anUsageTrack() {
            return new UsageTrackBuilder();
        }

        public UsageTrackBuilder withResourceType(String resourceType) {
            this.resourceType = resourceType;
            return this;
        }

        public UsageTrackBuilder withResourceCode(String resourceCode) {
            this.resourceCode = resourceCode;
            return this;
        }

        public UsageTrackBuilder withConsumerId(String consumerId) {
            this.consumerId = consumerId;
            return this;
        }

        public UsageTrackBuilder withEventId(String eventId) {
            this.eventId = eventId;
            return this;
        }

        public UsageTrackBuilder but() {
            return anUsageTrack()
                    .withResourceType(resourceType)
                    .withResourceCode(resourceCode)
                    .withConsumerId(consumerId)
                    .withEventId(eventId);
        }

        public UsageTrack build() {
            UsageTrack usageTrack = new UsageTrack();
            usageTrack.setResourceType(resourceType);
            usageTrack.setResourceCode(resourceCode);
            usageTrack.setConsumerId(consumerId);
            usageTrack.setEventId(eventId);
            return usageTrack;
        }
    }
}
