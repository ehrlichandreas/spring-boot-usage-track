package com.cognizant.ehrlichandreas.usagetrack.ressource;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.MoreObjects;
import java.util.Objects;

@JsonSerialize
@JsonDeserialize
public class UsageInformation {

    private String resourceType;

    private String resourceCode;

    private String consumerId;

    private Integer usagesConsumer;

    private Integer usagesTotal;

    public String getResourceType() {
        return resourceType;
    }

    public UsageInformation setResourceType(String resourceType) {
        this.resourceType = resourceType;
        return this;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public UsageInformation setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
        return this;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public UsageInformation setConsumerId(String consumerId) {
        this.consumerId = consumerId;
        return this;
    }

    public Integer getUsagesConsumer() {
        return usagesConsumer;
    }

    public UsageInformation setUsagesConsumer(Integer usagesConsumer) {
        this.usagesConsumer = usagesConsumer;
        return this;
    }

    public Integer getUsagesTotal() {
        return usagesTotal;
    }

    public UsageInformation setUsagesTotal(Integer usagesTotal) {
        this.usagesTotal = usagesTotal;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UsageInformation)) return false;
        UsageInformation that = (UsageInformation) o;
        return Objects.equals(getResourceType(), that.getResourceType())
                && Objects.equals(getResourceCode(), that.getResourceCode())
                && Objects.equals(getConsumerId(), that.getConsumerId())
                && Objects.equals(getUsagesConsumer(), that.getUsagesConsumer())
                && Objects.equals(getUsagesTotal(), that.getUsagesTotal());
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                getResourceType(),
                getResourceCode(),
                getConsumerId(),
                getUsagesConsumer(),
                getUsagesTotal());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("resourceType", resourceType)
                .add("resourceCode", resourceCode)
                .add("consumerId", consumerId)
                .add("usagesConsumer", usagesConsumer)
                .add("usagesTotal", usagesTotal)
                .toString();
    }
}
