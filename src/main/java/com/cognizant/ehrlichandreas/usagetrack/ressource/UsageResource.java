package com.cognizant.ehrlichandreas.usagetrack.ressource;

import static java.util.Objects.requireNonNull;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.cognizant.ehrlichandreas.usagetrack.service.ResourceUsageService;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** Endpoint for usage tracking */
@RestController
@RequestMapping("/api/resource-usages")
public class UsageResource {

    private final ResourceUsageService resourceUsageService;

    @Autowired
    public UsageResource(@NotNull final ResourceUsageService resourceUsageService) {
        this.resourceUsageService = requireNonNull(resourceUsageService);
    }

    @RequestMapping(
        method = GET,
        value = "/types/{resource-type}/codes/{resource-code}/consumers/{consumer-id}",
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public UsageInformation resourceUsages(
            @PathVariable("resource-type") String resourceType,
            @PathVariable("resource-code") String resourceCode,
            @PathVariable("consumer-id") String consumerId) {
        return resourceUsageService.getUsageInformation(resourceType, resourceCode, consumerId);
    }

    @RequestMapping(method = POST, value = "", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity incrementUsages(@RequestBody UsageTrack usageTrack) {
        if (resourceUsageService.incrementUsages(usageTrack)) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }

        return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }
}
