package com.cognizant.ehrlichandreas.usagetrack.service;

import static java.util.Objects.requireNonNull;

import com.cognizant.ehrlichandreas.usagetrack.persistence.repository.ResourceUsagesCountRepository;
import com.cognizant.ehrlichandreas.usagetrack.persistence.repository.ResourceUsagesRepository;
import com.cognizant.ehrlichandreas.usagetrack.persistence.repository.TotalUsagesRepository;
import com.cognizant.ehrlichandreas.usagetrack.ressource.UsageInformation;
import com.cognizant.ehrlichandreas.usagetrack.ressource.UsageTrack;
import java.util.Optional;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ResourceUsageService {

    /** Repository to save resource usages count. */
    private final ResourceUsagesCountRepository usagesCountRepository;

    /** Repository to save resource usages. */
    private final ResourceUsagesRepository usagesRepository;

    /** Repository to save total usages. */
    private final TotalUsagesRepository totalUsagesRepository;

    @Autowired
    public ResourceUsageService(
            @NotNull final ResourceUsagesRepository usagesRepository,
            @NotNull final ResourceUsagesCountRepository usagesCountRepository,
            @NotNull final TotalUsagesRepository totalUsagesRepository) {
        this.usagesRepository = requireNonNull(usagesRepository);
        this.usagesCountRepository = requireNonNull(usagesCountRepository);
        this.totalUsagesRepository = requireNonNull(totalUsagesRepository);
    }

    /**
     * Usage information contains the amount the resource was used in total and the usage by a
     * specific consumer.
     *
     * <p>If the consumer has never used the resource, 0 is returned.
     */
    public UsageInformation getUsageInformation(
            @NotNull final String resourceType,
            @NotNull String resourceCode,
            @NotNull String consumerId) {
        requireNonNull(resourceType);
        requireNonNull(resourceCode);
        requireNonNull(consumerId);

        return new UsageInformation()
                .setResourceType(resourceType)
                .setResourceCode(resourceCode)
                .setConsumerId(consumerId)
                .setUsagesConsumer(getConsumerUsages(resourceType, resourceCode, consumerId))
                .setUsagesTotal(getTotalUsages(resourceType, resourceCode));
    }

    /**
     * Increments consumer and total usages based on event id. Usages will not be incremented, if
     * the event is already registered.
     */
    @Transactional
    public Boolean incrementUsages(@NotNull final UsageTrack usageTrack) {
        requireNonNull(usageTrack);

        if (incrementResourceUsages(usageTrack)) {
            incrementResourceUsagesCount(
                    usageTrack.getResourceType(),
                    usageTrack.getResourceCode(),
                    usageTrack.getConsumerId());
            incrementTotalUsages(usageTrack.getResourceType(), usageTrack.getResourceCode());

            return true;
        }

        return false;
    }

    /**
     * Increments consumer resource usage and returns true.
     *
     * @return {@code true}, if the consumer's resource usage was incremented, {@code false}
     *     otherwise
     */
    private Boolean incrementResourceUsages(final UsageTrack usageTrack) {
        return Optional.ofNullable(
                                usagesRepository.create(
                                        usageTrack.getResourceType(),
                                        usageTrack.getResourceCode(),
                                        usageTrack.getConsumerId(),
                                        usageTrack.getEventId()))
                        .orElse(0)
                > 0;
    }

    /** Increments the resource usage counter. */
    private void incrementResourceUsagesCount(
            String resourceType, String resourceCode, String consumerId) {
        usagesCountRepository.increment(resourceType, resourceCode, consumerId);
    }

    /** Increments the total usage counter. */
    private void incrementTotalUsages(String resourceType, String resourceCode) {
        totalUsagesRepository.increment(resourceType, resourceCode);
    }

    /**
     * Returns the total consumer resource usage amount for provided resource type and code and
     * consumer id combination.
     */
    private Integer getConsumerUsages(String resourceType, String resourceCode, String consumerId) {
        return Optional.ofNullable(
                        usagesCountRepository.getUsagesCount(
                                resourceType, resourceCode, consumerId))
                .orElse(0);
    }

    /** Returns the total amount of events tracked for the specified resource type and code. */
    private Integer getTotalUsages(String resourceType, String resourceCode) {
        return Optional.ofNullable(totalUsagesRepository.getTotalUsages(resourceType, resourceCode))
                .orElse(0);
    }
}
