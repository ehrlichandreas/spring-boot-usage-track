package com.cognizant.ehrlichandreas.usagetrack.service;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.cognizant.ehrlichandreas.usagetrack.persistence.repository.ResourceUsagesCountRepository;
import com.cognizant.ehrlichandreas.usagetrack.persistence.repository.ResourceUsagesRepository;
import com.cognizant.ehrlichandreas.usagetrack.persistence.repository.TotalUsagesRepository;
import com.cognizant.ehrlichandreas.usagetrack.ressource.UsageTrack;
import com.cognizant.ehrlichandreas.usagetrack.ressource.UsageTrack.UsageTrackBuilder;
import org.junit.Before;
import org.junit.Test;

public class ResourceUsageServiceIncrementTest {

    private static final String RESOURCE_TYPE = "resourceType";
    private static final String RESOURCE_CODE = "resourceCode";
    private static final String CONSUMER_ID = "consumerId";
    private static final String EVENT_ID = "eventId";
    private static final String UNKNOWN_RESOURCE_TYPE = "unknown";
    private static final String UNKNOWN_RESOURCE_CODE = UNKNOWN_RESOURCE_TYPE;
    private static final String UNKNOWN_CONSUMER_ID = UNKNOWN_RESOURCE_CODE;

    private ResourceUsagesCountRepository usagesCountRepository;
    private ResourceUsagesRepository usagesRepository;
    private TotalUsagesRepository totalUsagesRepository;
    private ResourceUsageService resourceUsageService;

    @Before
    public void setUp() throws Exception {
        usagesCountRepository = mock(ResourceUsagesCountRepository.class);
        usagesRepository = mock(ResourceUsagesRepository.class);
        totalUsagesRepository = mock(TotalUsagesRepository.class);

        resourceUsageService =
                new ResourceUsageService(
                        usagesRepository, usagesCountRepository, totalUsagesRepository);

        when(usagesRepository.create(any(), any(), any(), any())).thenReturn(0);
        when(usagesRepository.create(
                        eq(RESOURCE_TYPE), eq(RESOURCE_CODE), eq(CONSUMER_ID), eq(EVENT_ID)))
                .thenReturn(1)
                .thenReturn(0);
    }

    @Test
    public void incrementsUsageAmountOnlyForDefaultUsage() {
        UsageTrack usageTrack =
                UsageTrackBuilder.anUsageTrack()
                        .withConsumerId(CONSUMER_ID)
                        .withEventId(EVENT_ID)
                        .withResourceCode(RESOURCE_CODE)
                        .withResourceType(RESOURCE_TYPE)
                        .build();

        resourceUsageService.incrementUsages(usageTrack);

        verify(usagesRepository, never())
                .create(UNKNOWN_RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID, EVENT_ID);
        verify(usagesRepository, times(1))
                .create(RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID, EVENT_ID);

        verify(usagesCountRepository, never())
                .increment(UNKNOWN_RESOURCE_TYPE, UNKNOWN_RESOURCE_CODE, UNKNOWN_CONSUMER_ID);
        verify(usagesCountRepository, times(1))
                .increment(RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID);

        verify(totalUsagesRepository, never())
                .increment(UNKNOWN_RESOURCE_TYPE, UNKNOWN_RESOURCE_CODE);
        verify(totalUsagesRepository, times(1)).increment(RESOURCE_TYPE, RESOURCE_CODE);

        verifyNoMoreInteractions(usagesRepository);
        verifyNoMoreInteractions(usagesCountRepository);
        verifyNoMoreInteractions(totalUsagesRepository);
    }

    @Test
    public void incrementNotOnSecondIncrementCall() {
        UsageTrack usageTrack =
                UsageTrackBuilder.anUsageTrack()
                        .withConsumerId(CONSUMER_ID)
                        .withEventId(EVENT_ID)
                        .withResourceCode(RESOURCE_CODE)
                        .withResourceType(RESOURCE_TYPE)
                        .build();

        resourceUsageService.incrementUsages(usageTrack);
        resourceUsageService.incrementUsages(usageTrack);

        verify(usagesRepository, never())
                .create(UNKNOWN_RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID, EVENT_ID);
        verify(usagesRepository, times(2))
                .create(RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID, EVENT_ID);

        verify(usagesCountRepository, never())
                .increment(UNKNOWN_RESOURCE_TYPE, UNKNOWN_RESOURCE_CODE, UNKNOWN_CONSUMER_ID);
        verify(usagesCountRepository, times(1))
                .increment(RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID);

        verify(totalUsagesRepository, never())
                .increment(UNKNOWN_RESOURCE_TYPE, UNKNOWN_RESOURCE_CODE);
        verify(totalUsagesRepository, times(1)).increment(RESOURCE_TYPE, RESOURCE_CODE);

        verifyNoMoreInteractions(usagesRepository);
        verifyNoMoreInteractions(usagesCountRepository);
        verifyNoMoreInteractions(totalUsagesRepository);
    }
}
