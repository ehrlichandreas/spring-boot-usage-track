package com.cognizant.ehrlichandreas.usagetrack.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.cognizant.ehrlichandreas.usagetrack.persistence.repository.ResourceUsagesCountRepository;
import com.cognizant.ehrlichandreas.usagetrack.persistence.repository.ResourceUsagesRepository;
import com.cognizant.ehrlichandreas.usagetrack.persistence.repository.TotalUsagesRepository;
import com.cognizant.ehrlichandreas.usagetrack.ressource.UsageInformation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ResourceUsageServiceGetTest {

    private static final String UNKNOWN_RESOURCE_TYPE = "unknown";
    private static final String UNKNOWN_RESOURCE_CODE = UNKNOWN_RESOURCE_TYPE;
    private static final String UNKNOWN_CONSUMER_ID = UNKNOWN_RESOURCE_CODE;
    private static final String RESOURCE_TYPE = "resourceType";
    private static final String RESOURCE_CODE = "resourceCode";
    private static final String CONSUMER_ID = "consumerId";

    private ResourceUsagesCountRepository usagesCountRepository;
    private ResourceUsagesRepository usagesRepository;
    private TotalUsagesRepository totalUsagesRepository;
    private ResourceUsageService resourceUsageService;

    @Before
    public void setUp() throws Exception {
        usagesCountRepository = mock(ResourceUsagesCountRepository.class);
        usagesRepository = mock(ResourceUsagesRepository.class);
        totalUsagesRepository = mock(TotalUsagesRepository.class);

        resourceUsageService =
                new ResourceUsageService(
                        usagesRepository, usagesCountRepository, totalUsagesRepository);

        when(usagesCountRepository.getUsagesCount(any(), any(), any())).thenReturn(0);

        when(usagesCountRepository.getUsagesCount(
                        eq(RESOURCE_TYPE), eq(RESOURCE_CODE), eq(CONSUMER_ID)))
                .thenReturn(3);

        when(totalUsagesRepository.getTotalUsages(any(), any())).thenReturn(0);

        when(totalUsagesRepository.getTotalUsages(eq(RESOURCE_TYPE), eq(RESOURCE_CODE)))
                .thenReturn(7);
    }

    @Test
    public void returnsDefinedUsageAmount() {
        UsageInformation actual =
                resourceUsageService.getUsageInformation(RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID);

        verify(usagesCountRepository, never())
                .getUsagesCount(RESOURCE_TYPE, RESOURCE_CODE, UNKNOWN_CONSUMER_ID);
        verify(usagesCountRepository, times(1))
                .getUsagesCount(RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID);
        verify(totalUsagesRepository, never()).getTotalUsages(RESOURCE_TYPE, UNKNOWN_RESOURCE_CODE);
        verify(totalUsagesRepository, times(1)).getTotalUsages(RESOURCE_TYPE, RESOURCE_CODE);

        verifyNoMoreInteractions(usagesRepository);
        verifyNoMoreInteractions(usagesCountRepository);
        verifyNoMoreInteractions(totalUsagesRepository);

        UsageInformation expected =
                new UsageInformation()
                        .setConsumerId(CONSUMER_ID)
                        .setResourceCode(RESOURCE_CODE)
                        .setResourceType(RESOURCE_TYPE)
                        .setUsagesConsumer(3)
                        .setUsagesTotal(7);

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void returnsCorrectUsageAmountMap() {
        UsageInformation actual =
                resourceUsageService.getUsageInformation(
                        RESOURCE_TYPE, RESOURCE_CODE, UNKNOWN_CONSUMER_ID);

        verify(usagesCountRepository, never())
                .getUsagesCount(RESOURCE_TYPE, RESOURCE_CODE, CONSUMER_ID);
        verify(usagesCountRepository, times(1))
                .getUsagesCount(RESOURCE_TYPE, RESOURCE_CODE, UNKNOWN_CONSUMER_ID);
        verify(totalUsagesRepository, never()).getTotalUsages(RESOURCE_TYPE, UNKNOWN_RESOURCE_CODE);
        verify(totalUsagesRepository, times(1)).getTotalUsages(RESOURCE_TYPE, RESOURCE_CODE);

        verifyNoMoreInteractions(usagesRepository);
        verifyNoMoreInteractions(usagesCountRepository);
        verifyNoMoreInteractions(totalUsagesRepository);

        UsageInformation expected =
                new UsageInformation()
                        .setConsumerId(UNKNOWN_CONSUMER_ID)
                        .setResourceCode(RESOURCE_CODE)
                        .setResourceType(RESOURCE_TYPE)
                        .setUsagesConsumer(0)
                        .setUsagesTotal(7);

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void returnsZeroUsageOnNotFoundResource() {
        UsageInformation actual =
                resourceUsageService.getUsageInformation(
                        RESOURCE_TYPE, UNKNOWN_RESOURCE_CODE, UNKNOWN_CONSUMER_ID);

        verify(usagesCountRepository, never())
                .getUsagesCount(RESOURCE_TYPE, RESOURCE_CODE, UNKNOWN_CONSUMER_ID);
        verify(usagesCountRepository, times(1))
                .getUsagesCount(RESOURCE_TYPE, UNKNOWN_RESOURCE_CODE, UNKNOWN_CONSUMER_ID);
        verify(totalUsagesRepository, never()).getTotalUsages(RESOURCE_TYPE, RESOURCE_CODE);
        verify(totalUsagesRepository, times(1))
                .getTotalUsages(RESOURCE_TYPE, UNKNOWN_RESOURCE_CODE);

        verifyNoMoreInteractions(usagesRepository);
        verifyNoMoreInteractions(usagesCountRepository);
        verifyNoMoreInteractions(totalUsagesRepository);

        UsageInformation expected =
                new UsageInformation()
                        .setConsumerId(UNKNOWN_CONSUMER_ID)
                        .setResourceCode(UNKNOWN_RESOURCE_CODE)
                        .setResourceType(RESOURCE_TYPE)
                        .setUsagesConsumer(0)
                        .setUsagesTotal(0);

        Assert.assertEquals(actual, expected);
    }
}
