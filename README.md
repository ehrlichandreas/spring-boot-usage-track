# Usage track

Service for tracking resource usages.

Staging: https://staging-url.com

Production: https://production-url.com

## Building

### A fat-jar
Use `./gradlew clean check bootRepackage` to build a fat-jar file

### A docker container
Use `./gradlew clean check distDocker` to build a docker image

### Running locally
Use `SPRING_PROFILES_ACTIVE=staging ./gradlew clean check bootRun` to run it
locally

## Configuration
Configuration values are read from the OS environment
